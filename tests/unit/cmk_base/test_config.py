# encoding: utf-8
# pylint: disable=redefined-outer-name

import pytest  # type: ignore
from testlib.base import Scenario

from cmk.utils.rulesets.ruleset_matcher import RulesetMatchObject
import cmk.utils.paths
import cmk_base.config as config
import cmk_base.piggyback as piggyback


def test_all_configured_realhosts(monkeypatch):
    ts = Scenario(site_id="site1")
    ts.add_host("real1", ["site:site1"])
    ts.add_host("real2", ["site:site2"])
    ts.add_host("real3", [])
    ts.add_cluster("cluster1", ["site:site1"], nodes=["node1"])
    ts.add_cluster("cluster2", ["site:site2"], nodes=["node2"])
    ts.add_cluster("cluster3", [], nodes=["node3"])

    config_cache = ts.apply(monkeypatch)
    assert config_cache.all_configured_clusters() == set(["cluster1", "cluster2", "cluster3"])
    assert config_cache.all_configured_realhosts() == set(["real1", "real2", "real3"])
    assert config_cache.all_configured_hosts() == set(
        ["cluster1", "cluster2", "cluster3", "real1", "real2", "real3"])


@pytest.mark.parametrize("hostname,tags,result", [
    ("testhost", [], True),
    ("testhost", ["ip-v4"], True),
    ("testhost", ["ip-v4", "ip-v6"], True),
    ("testhost", ["ip-v6"], False),
    ("testhost", ["no-ip"], False),
])
def test_is_ipv4_host(monkeypatch, hostname, tags, result):
    config_cache = Scenario().add_host(hostname, tags).apply(monkeypatch)
    assert config_cache.get_host_config(hostname).is_ipv4_host == result


@pytest.mark.parametrize("hostname,tags,result", [
    ("testhost", [], False),
    ("testhost", ["ip-v4"], False),
    ("testhost", ["ip-v4", "ip-v6"], True),
    ("testhost", ["ip-v6"], True),
    ("testhost", ["no-ip"], False),
])
def test_is_ipv6_host(monkeypatch, hostname, tags, result):
    config_cache = Scenario().add_host(hostname, tags).apply(monkeypatch)
    assert config_cache.get_host_config(hostname).is_ipv6_host == result


@pytest.mark.parametrize("hostname,tags,result", [
    ("testhost", [], False),
    ("testhost", ["ip-v4"], False),
    ("testhost", ["ip-v4", "ip-v6"], True),
    ("testhost", ["ip-v6"], False),
    ("testhost", ["no-ip"], False),
])
def test_is_ipv4v6_host(monkeypatch, hostname, tags, result):
    config_cache = Scenario().add_host(hostname, tags).apply(monkeypatch)
    assert config_cache.get_host_config(hostname).is_ipv4v6_host == result


@pytest.mark.parametrize("hostname,tags,result", [
    ("testhost", ["piggyback"], True),
    ("testhost", ["no-piggyback"], False),
])
def test_is_piggyback_host(monkeypatch, hostname, tags, result):
    config_cache = Scenario().add_host(hostname, tags).apply(monkeypatch)
    assert config_cache.get_host_config(hostname).is_piggyback_host == result


@pytest.mark.parametrize("with_data,result", [
    (True, True),
    (False, False),
])
@pytest.mark.parametrize("hostname,tags", [
    ("testhost", []),
    ("testhost", ["auto-piggyback"]),
])
def test_is_piggyback_host_auto(monkeypatch, hostname, tags, with_data, result):
    monkeypatch.setattr(piggyback, "has_piggyback_raw_data", lambda cache_age, hostname: with_data)
    config_cache = Scenario().add_host(hostname, tags).apply(monkeypatch)
    assert config_cache.get_host_config(hostname).is_piggyback_host == result


@pytest.mark.parametrize("hostname,tags,result", [
    ("testhost", [], False),
    ("testhost", ["ip-v4"], False),
    ("testhost", ["ip-v4", "ip-v6"], False),
    ("testhost", ["ip-v6"], False),
    ("testhost", ["no-ip"], True),
])
def test_is_no_ip_host(monkeypatch, hostname, tags, result):
    config_cache = Scenario().add_host(hostname, tags).apply(monkeypatch)
    assert config_cache.get_host_config(hostname).is_no_ip_host == result


@pytest.mark.parametrize("hostname,tags,result,ruleset", [
    ("testhost", [], False, []),
    ("testhost", ["ip-v4"], False, [
        ('ipv6', [], config.ALL_HOSTS, {}),
    ]),
    ("testhost", ["ip-v4", "ip-v6"], False, []),
    ("testhost", ["ip-v4", "ip-v6"], True, [
        ('ipv6', [], config.ALL_HOSTS, {}),
    ]),
    ("testhost", ["ip-v6"], True, []),
    ("testhost", ["ip-v6"], True, [
        ('ipv4', [], config.ALL_HOSTS, {}),
    ]),
    ("testhost", ["ip-v6"], True, [
        ('ipv6', [], config.ALL_HOSTS, {}),
    ]),
    ("testhost", ["no-ip"], False, []),
])
def test_is_ipv6_primary_host(monkeypatch, hostname, tags, result, ruleset):
    ts = Scenario().add_host(hostname, tags)
    ts.set_ruleset("primary_address_family", ruleset)
    config_cache = ts.apply(monkeypatch)
    assert config_cache.get_host_config(hostname).is_ipv6_primary == result


@pytest.mark.parametrize("result,attrs", [
    ("127.0.1.1", {}),
    ("127.0.1.1", {
        "management_address": ""
    }),
    ("127.0.0.1", {
        "management_address": "127.0.0.1"
    }),
    ("lolo", {
        "management_address": "lolo"
    }),
])
def test_management_address_of(monkeypatch, attrs, result):
    # Host IP address is 127.0.1.1
    monkeypatch.setitem(config.ipaddresses, "hostname", "127.0.1.1")

    monkeypatch.setitem(config.host_attributes, "hostname", attrs)

    assert config.management_address_of("hostname") == result


@pytest.mark.parametrize("hostname,tags,result", [
    ("testhost", [], True),
    ("testhost", ["cmk-agent"], True),
    ("testhost", ["cmk-agent", "tcp"], True),
    ("testhost", ["snmp", "tcp"], True),
    ("testhost", ["ping"], False),
    ("testhost", ["no-agent", "no-snmp"], False),
])
def test_is_tcp_host(monkeypatch, hostname, tags, result):
    config_cache = Scenario().add_host(hostname, tags).apply(monkeypatch)
    assert config_cache.get_host_config(hostname).is_tcp_host == result


@pytest.mark.parametrize("hostname,tags,result", [
    ("testhost", [], False),
    ("testhost", ["cmk-agent"], False),
    ("testhost", ["snmp", "tcp"], False),
    ("testhost", ["snmp", "tcp", "ping"], False),
    ("testhost", ["snmp"], False),
    ("testhost", ["no-agent", "no-snmp", "no-piggyback"], True),
    ("testhost", ["no-agent", "no-snmp"], True),
    ("testhost", ["ping"], True),
])
def test_is_ping_host(monkeypatch, hostname, tags, result):
    config_cache = Scenario().add_host(hostname, tags).apply(monkeypatch)
    assert config_cache.get_host_config(hostname).is_ping_host == result


@pytest.mark.parametrize("hostname,tags,result", [
    ("testhost", [], False),
    ("testhost", ["cmk-agent"], False),
    ("testhost", ["snmp", "tcp"], True),
    ("testhost", ["snmp"], True),
])
def test_is_snmp_host(monkeypatch, hostname, tags, result):
    config_cache = Scenario().add_host(hostname, tags).apply(monkeypatch)
    assert config_cache.get_host_config(hostname).is_snmp_host == result


def test_is_not_usewalk_host(monkeypatch):
    config_cache = Scenario().add_host("xyz", ["abc"]).apply(monkeypatch)
    assert config_cache.get_host_config("xyz").is_usewalk_host is False


def test_is_usewalk_host(monkeypatch):
    ts = Scenario()
    ts.add_host("xyz", ["abc"])
    ts.set_ruleset("usewalk_hosts", [
        (["xyz"], config.ALL_HOSTS, {}),
    ])

    config_cache = ts.apply(monkeypatch)
    assert config_cache.get_host_config("xyz").is_usewalk_host is False


@pytest.mark.parametrize("hostname,tags,result", [
    ("testhost", [], False),
    ("testhost", ["tcp"], False),
    ("testhost", ["snmp"], False),
    ("testhost", ["cmk-agent", "snmp"], False),
    ("testhost", ["no-agent", "no-snmp"], False),
    ("testhost", ["tcp", "snmp"], True),
])
def test_is_dual_host(monkeypatch, hostname, tags, result):
    config_cache = Scenario().add_host(hostname, tags).apply(monkeypatch)
    assert config_cache.get_host_config(hostname).is_dual_host == result


@pytest.mark.parametrize("hostname,tags,result", [
    ("testhost", [], False),
    ("testhost", ["all-agents"], True),
    ("testhost", ["special-agents"], False),
    ("testhost", ["no-agent"], False),
    ("testhost", ["cmk-agent"], False),
])
def test_is_all_agents_host(monkeypatch, hostname, tags, result):
    config_cache = Scenario().add_host(hostname, tags).apply(monkeypatch)
    assert config_cache.get_host_config(hostname).is_all_agents_host == result


@pytest.mark.parametrize("hostname,tags,result", [
    ("testhost", [], False),
    ("testhost", ["all-agents"], False),
    ("testhost", ["special-agents"], True),
    ("testhost", ["no-agent"], False),
    ("testhost", ["cmk-agent"], False),
])
def test_is_all_special_agents_host(monkeypatch, hostname, tags, result):
    config_cache = Scenario().add_host(hostname, tags).apply(monkeypatch)
    assert config_cache.get_host_config(hostname).is_all_special_agents_host == result


def test_prepare_check_command_basics():
    assert config.prepare_check_command(u"args 123 -x 1 -y 2", "bla", "blub") \
        == u"args 123 -x 1 -y 2"

    assert config.prepare_check_command(["args", "123", "-x", "1", "-y", "2"], "bla", "blub") \
        == "'args' '123' '-x' '1' '-y' '2'"

    assert config.prepare_check_command(["args", "1 2 3", "-d=2", "--hallo=eins", 9], "bla", "blub") \
        == "'args' '1 2 3' '-d=2' '--hallo=eins' 9"

    with pytest.raises(NotImplementedError):
        config.prepare_check_command((1, 2), "bla", "blub")


@pytest.mark.parametrize("pw", ["abc", "123", "x'äd!?", u"aädg"])
def test_prepare_check_command_password_store(monkeypatch, pw):
    monkeypatch.setattr(config, "stored_passwords", {"pw-id": {"password": pw,}})
    assert config.prepare_check_command(["arg1", ("store", "pw-id", "--password=%s"), "arg3"], "bla", "blub") \
        == "--pwstore=2@11@pw-id 'arg1' '--password=%s' 'arg3'" % ("*" * len(pw))


def test_prepare_check_command_not_existing_password(capsys):
    assert config.prepare_check_command(["arg1", ("store", "pw-id", "--password=%s"), "arg3"], "bla", "blub") \
        == "--pwstore=2@11@pw-id 'arg1' '--password=***' 'arg3'"
    stderr = capsys.readouterr().err
    assert "The stored password \"pw-id\" used by service \"blub\" on host \"bla\"" in stderr


def test_http_proxies():
    assert config.http_proxies == {}


@pytest.mark.parametrize("http_proxy,result", [
    ("bla", None),
    (("no_proxy", None), ""),
    (("environment", None), None),
    (("global", "not_existing"), None),
    (("global", "http_blub"), "http://blub:8080"),
    (("global", "https_blub"), "https://blub:8181"),
    (("global", "socks5_authed"), "socks5://us%3Aer:s%40crit@socks.proxy:443"),
    (("url", "http://8.4.2.1:1337"), "http://8.4.2.1:1337"),
])
def test_http_proxy(http_proxy, result, monkeypatch):
    monkeypatch.setattr(
        config, "http_proxies", {
            "http_blub": {
                "ident": "blub",
                "title": "HTTP blub",
                "proxy_url": "http://blub:8080",
            },
            "https_blub": {
                "ident": "blub",
                "title": "HTTPS blub",
                "proxy_url": "https://blub:8181",
            },
            "socks5_authed": {
                "ident": "socks5",
                "title": "HTTP socks5 authed",
                "proxy_url": "socks5://us%3Aer:s%40crit@socks.proxy:443",
            },
        })

    assert config.get_http_proxy(http_proxy) == result


def test_service_depends_on_unknown_host():
    assert config.service_depends_on("test-host", "svc") == []


def test_service_depends_on(monkeypatch):
    ts = Scenario().add_host("test-host", tags=[])
    ts.set_ruleset("service_dependencies", [
        ("dep1", [], config.ALL_HOSTS, ["svc1"], {}),
        ("dep2-%s", [], config.ALL_HOSTS, ["svc1-(.*)"], {}),
        ("dep-disabled", [], config.ALL_HOSTS, ["svc1"], {
            "disabled": True
        }),
    ])
    ts.apply(monkeypatch)

    assert config.service_depends_on("test-host", "svc2") == []
    assert config.service_depends_on("test-host", "svc1") == ["dep1"]
    assert config.service_depends_on("test-host", "svc1-abc") == ["dep1", "dep2-abc"]


@pytest.fixture()
def cluster_config(monkeypatch):
    ts = Scenario().add_host("node1").add_host("host1")
    ts.add_cluster("cluster1", nodes=["node1"])
    return ts.apply(monkeypatch)


def test_host_config_is_cluster(cluster_config):
    assert cluster_config.get_host_config("node1").is_cluster is False
    assert cluster_config.get_host_config("host1").is_cluster is False
    assert cluster_config.get_host_config("cluster1").is_cluster is True


def test_host_config_part_of_clusters(cluster_config):
    assert cluster_config.get_host_config("node1").part_of_clusters == ["cluster1"]
    assert cluster_config.get_host_config("host1").part_of_clusters == []
    assert cluster_config.get_host_config("cluster1").part_of_clusters == []


def test_host_config_nodes(cluster_config):
    assert cluster_config.get_host_config("node1").nodes is None
    assert cluster_config.get_host_config("host1").nodes is None
    assert cluster_config.get_host_config("cluster1").nodes == ["node1"]


def test_host_config_parents(cluster_config):
    assert cluster_config.get_host_config("node1").parents == []
    assert cluster_config.get_host_config("host1").parents == []
    # TODO: Move cluster/node parent handling to HostConfig
    #assert cluster_config.get_host_config("cluster1").parents == ["node1"]
    assert cluster_config.get_host_config("cluster1").parents == []


def test_host_tags_default():
    assert isinstance(config.host_tags, dict)


def test_host_tags_of_host(monkeypatch):
    ts = Scenario()
    ts.add_host("test-host", ["abc"])
    ts.set_option("host_tags", {
        "test-host": {
            "tag_group": "abc",
        },
    })
    config_cache = ts.apply(monkeypatch)

    cfg = config_cache.get_host_config("xyz")
    assert cfg.tag_groups == {}
    assert config_cache.tags_of_host("xyz") == {}

    cfg = config_cache.get_host_config("test-host")
    assert cfg.tag_groups == {"tag_group": "abc"}
    assert config_cache.tags_of_host("test-host") == {"tag_group": "abc"}


def test_service_tag_rules_default():
    assert isinstance(config.service_tag_rules, list)


def test_tags_of_service(monkeypatch):
    ts = Scenario()
    ts.set_option("host_tags", {
        "test-host": {
            "tag_group": "abc",
        },
    })

    ts.set_ruleset("service_tag_rules", [
        ([("tag_group1", "val1")], ["abc"], config.ALL_HOSTS, ["CPU load$"], {}),
    ])

    ts.add_host("test-host", ["abc"])
    config_cache = ts.apply(monkeypatch)

    cfg = config_cache.get_host_config("xyz")
    assert cfg.tag_groups == {}
    assert config_cache.tags_of_service("xyz", "CPU load") == {}

    cfg = config_cache.get_host_config("test-host")
    assert cfg.tag_groups == {"tag_group": "abc"}
    assert config_cache.tags_of_service("test-host", "CPU load") == {"tag_group1": "val1"}


def test_host_label_rules_default():
    assert isinstance(config.host_label_rules, list)


def test_host_config_labels(monkeypatch):
    ts = Scenario()
    ts.set_option("host_labels", {
        "test-host": {
            "explicit": "ding",
        },
    })

    ts.set_ruleset("host_label_rules", [
        ({
            "from-rule": "rule1"
        }, ["abc"], config.ALL_HOSTS, {}),
        ({
            "from-rule2": "rule2"
        }, ["abc"], config.ALL_HOSTS, {}),
    ])

    ts.add_host("test-host", ["abc"])

    config_cache = ts.apply(monkeypatch)

    cfg = config_cache.get_host_config("xyz")
    assert cfg.labels == {}

    cfg = config_cache.get_host_config("test-host")
    assert cfg.labels == {
        "explicit": "ding",
        "from-rule": "rule1",
        "from-rule2": "rule2",
    }
    assert cfg.label_sources == {
        "explicit": "explicit",
        "from-rule": "ruleset",
        "from-rule2": "ruleset",
    }


def test_host_labels_of_host_discovered_labels(monkeypatch, tmp_path):
    ts = Scenario().add_host("test-host", ["abc"])

    monkeypatch.setattr(cmk.utils.paths, "discovered_host_labels_dir", tmp_path)
    host_file = (tmp_path / "test-host").with_suffix(".mk")
    with host_file.open(mode="wb") as f:
        f.write(repr({u"äzzzz": u"eeeeez"}) + "\n")

    config_cache = ts.apply(monkeypatch)
    assert config_cache.get_host_config("test-host").labels == {u"äzzzz": u"eeeeez"}
    assert config_cache.get_host_config("test-host").label_sources == {u"äzzzz": u"discovered"}


def test_service_label_rules_default():
    assert isinstance(config.service_label_rules, list)


def test_labels_of_service(monkeypatch):
    ts = Scenario()
    ts.set_ruleset("service_label_rules", [
        ({
            "label1": "val1"
        }, ["abc"], config.ALL_HOSTS, ["CPU load$"], {}),
        ({
            "label2": "val2"
        }, ["abc"], config.ALL_HOSTS, ["CPU load$"], {}),
    ])

    ts.add_host("test-host", ["abc"])
    config_cache = ts.apply(monkeypatch)

    assert config_cache.labels_of_service("xyz", "CPU load") == {}
    assert config_cache.label_sources_of_service("xyz", "CPU load") == {}

    assert config_cache.labels_of_service("test-host", "CPU load") == {
        "label1": "val1",
        "label2": "val2",
    }
    assert config_cache.label_sources_of_service("test-host", "CPU load") == {
        "label1": "ruleset",
        "label2": "ruleset",
    }


def test_config_cache_get_host_config():
    cache = config.ConfigCache()
    assert cache._host_configs == {}

    host_config = cache.get_host_config("xyz")
    assert isinstance(host_config, config.HostConfig)
    assert host_config is cache.get_host_config("xyz")


def test_host_ruleset_match_object_of_host(monkeypatch):
    ts = Scenario()
    ts.set_option("host_tags", {
        "test-host": {
            "tag_group": "abc",
        },
    })
    ts.add_host("test-host", ["abc"])
    config_cache = ts.apply(monkeypatch)

    cfg = config_cache.get_host_config("xyz")
    assert isinstance(cfg.ruleset_match_object, RulesetMatchObject)
    assert cfg.ruleset_match_object.to_dict() == {
        "host_tags": {},
        "host_name": "xyz",
    }

    cfg = config_cache.get_host_config("test-host")
    assert isinstance(cfg.ruleset_match_object, RulesetMatchObject)
    assert cfg.ruleset_match_object.to_dict() == {
        "host_name": "test-host",
        "host_tags": {
            "tag_group": "abc",
        }
    }


def test_host_ruleset_match_object_of_service(monkeypatch):
    ts = Scenario()
    ts.set_option("host_tags", {
        "test-host": {
            "tag_group": "abc",
        },
    })
    ts.add_host("test-host", ["abc"])
    config_cache = ts.apply(monkeypatch)

    obj = config_cache.ruleset_match_object_of_service("xyz", "bla blä")
    assert isinstance(obj, RulesetMatchObject)
    assert obj.to_dict() == {
        "host_name": "xyz",
        "host_tags": {},
        "service_description": "bla blä",
    }

    obj = config_cache.ruleset_match_object_of_service("test-host", "CPU load")
    assert isinstance(obj, RulesetMatchObject)
    assert obj.to_dict() == {
        "host_name": "test-host",
        "host_tags": {
            "tag_group": "abc",
        },
        "service_description": "CPU load",
    }


@pytest.mark.parametrize("result,ruleset", [
    (False, None),
    (False, []),
    (False, [(None, [], config.ALL_HOSTS, {})]),
    (False, [({}, [], config.ALL_HOSTS, {})]),
    (True, [({
        "status_data_inventory": True
    }, [], config.ALL_HOSTS, {})]),
    (False, [({
        "status_data_inventory": False
    }, [], config.ALL_HOSTS, {})]),
])
def test_do_status_data_inventory_for(monkeypatch, result, ruleset):
    ts = Scenario().add_host("abc", [])
    ts.set_option("active_checks", {
        "cmk_inv": ruleset,
    })
    ts.apply(monkeypatch)

    assert config.do_status_data_inventory_for("abc") == result


@pytest.mark.parametrize("result,ruleset", [
    (True, None),
    (True, []),
    (True, [(None, [], config.ALL_HOSTS, {})]),
    (True, [({}, [], config.ALL_HOSTS, {})]),
    (True, [({
        "host_label_inventory": True
    }, [], config.ALL_HOSTS, {})]),
    (False, [({
        "host_label_inventory": False
    }, [], config.ALL_HOSTS, {})]),
])
def test_do_host_label_discovery_for(monkeypatch, result, ruleset):
    ts = Scenario().add_host("abc", [])
    ts.set_option("active_checks", {
        "cmk_inv": ruleset,
    })
    ts.apply(monkeypatch)

    assert config.do_host_label_discovery_for("abc") == result
